# LavCorps-Line Obsidius Dockers

Inspired by [itzg/docker-minecraft-server](https://github.com/itzg/docker-minecraft-server),
Obsidius dockers are intended to ease Minecraft server management,
allowing always-up-to-date minecraft servers,
or a server fixed to a particular version.

A catch-all docker-compose will be provided,
as well as individual dockers for Vanilla,
Fabric,
Paper,
Quilt,
and Purpur servers.
